This plugin allows the Tuckey UrlRewriteFilter to preempt dotCMS' CMSFilter so that Tuckey is allowed to rewrite any URL.

This plugin places the UrlRewriteFilter <filter-mapping> element further down the web.xml configuration file so that the UrlRewriteFilter has precedence in the servlet filter chain.